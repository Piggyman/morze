def code_morze(value):
    morze_dict = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..',
                  'E': '.', 'F': '..-.', 'G': '--.', 'H': '....',
                  'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.',
                  'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
                  'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..',
                  '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....',
                  '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--',
                  '.': '.-.-.-',  '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'}
    massage_upper = value.upper() #normalisation string chracters to dictionary keys
    massage_morze = []
    for char in massage_upper:
        if char in morze_dict: #compare chracters to dictionary keys
            massage_morze.append(morze_dict[char])
    return ' '.join(massage_morze) #convert list to string
